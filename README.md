![SNAX](https://git.ligo.org/uploads/-/system/project/avatar/4921/waveform_final1.png "SNAX")

Stream-based Noise Acquisition and eXtraction analysis toolkit
==========================================================================

[![pipeline status](https://git.ligo.org/snax/snax/badges/main/pipeline.svg)](https://git.ligo.org/snax/snax/commits/main)
[![coverage report](https://git.ligo.org/snax/snax/badges/main/coverage.svg)](https://git.ligo.org/snax/snax/commits/main)
[![conda-forge](https://img.shields.io/conda/vn/conda-forge/snax.svg)](https://anaconda.org/conda-forge/snax)

|              |        |
| ------------ | ------ |
| **Web:**     | https://docs.ligo.org/snax/snax  |
