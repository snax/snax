import pytest


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_aggregate(script_runner):
    ret = script_runner.run("snax_aggregate", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_bank_overlap(script_runner):
    ret = script_runner.run("snax_bank_overlap", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_combine(script_runner):
    ret = script_runner.run("snax_combine", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_extract(script_runner):
    ret = script_runner.run("snax_extract", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_generate(script_runner):
    ret = script_runner.run("snax_generate", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_monitor(script_runner):
    ret = script_runner.run("snax_monitor", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_sink(script_runner):
    ret = script_runner.run("snax_sink", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_synchronize(script_runner):
    ret = script_runner.run("snax_synchronize", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_whiten(script_runner):
    ret = script_runner.run("snax_whiten", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_workflow(script_runner):
    ret = script_runner.run("snax_workflow", "--help")
    assert ret.success


@pytest.mark.script_launch_mode("subprocess")
def test_cli_snax_workflow_online(script_runner):
    ret = script_runner.run("snax_workflow_online", "--help")
    assert ret.success
