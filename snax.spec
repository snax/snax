%define name              snax
%define version           0.5.2
%define unmangled_version 0.5.2
%define release           1

Summary:   Stream-based Noise Acquisition and eXtraction toolkit
Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Source0:   http://software.igwn.org/lscsoft/source/%{name}-%{unmangled_version}.tar.gz
License:   GPLv2+
Group:     Development/Libraries
Prefix:    %{_prefix}
Vendor:    Patrick Godwin <patrick.godwin@ligo.org>
Url:       https://git.ligo.org/snax/snax

BuildArch: noarch

BuildRequires: rpm-build
BuildRequires: epel-rpm-macros

# python3-snax
BuildRequires: python3-rpm-macros
BuildRequires: python%{python3_pkgversion}
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-setuptools_scm

# -- snax

Requires: python%{python3_pkgversion}-snax = %{version}-%{release}

%description
SNAX is a Stream-based Noise Acquisition and eXtraction toolkit to extract
features from timeseries data.  This package provides the SNAX executables.

# -- python3-snax

%package -n python%{python3_pkgversion}-%{name}
Summary:  %{summary}
Requires: gstlal
Requires: gstlal-ugly
Requires: python%{python3_pkgversion}-gstreamer1
Requires: python%{python3_pkgversion}-gobject
Requires: python%{python3_pkgversion}-h5py
Requires: python%{python3_pkgversion}-jinja2
Requires: python%{python3_pkgversion}-lal
Requires: python%{python3_pkgversion}-ligo-scald
Requires: python%{python3_pkgversion}-ligo-segments
Requires: python%{python3_pkgversion}-numpy
Requires: python%{python3_pkgversion}-pluggy
Requires: python%{python3_pkgversion}-ligo-lw
Requires: python%{python3_pkgversion}-scipy
Requires: python3-matplotlib

%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}

%description -n python%{python3_pkgversion}-%{name}
SNAX is a Stream-based Noise Acquisition and eXtraction toolkit to extract
features from timeseries data.  This package provides the Python
%{python3_version} library.

# -- build steps

%prep
%setup -n %{name}-%{unmangled_version}

%build
%py3_build

%install
%py3_install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%license LICENSE
%{_bindir}/snax_*

%files -n python%{python3_pkgversion}-%{name}
%license LICENSE
%{python3_sitelib}/*
